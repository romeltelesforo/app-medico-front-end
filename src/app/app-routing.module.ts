import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PacienteComponent } from 'src/app/pages/paciente/paciente.component';
import { PacienteEdicionComponent } from 'src/app/pages/paciente/paciente-edicion/paciente-edicion.component';
import { MedicoComponent } from 'src/app/pages/medico/medico.component';

const routes: Routes = [
  {
    path: "paciente", component:PacienteComponent, children:[
      {path:'nuevo', component: PacienteEdicionComponent},
      {path:'edicion/:id', component:PacienteEdicionComponent},
    ]
  },
  {path: "medico", component:MedicoComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
